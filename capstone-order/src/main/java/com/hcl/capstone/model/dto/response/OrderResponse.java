package com.hcl.capstone.model.dto.response;

import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@Builder
public class OrderResponse {
    private Integer orderId;
    private Integer userId;
    private List<OrderDetailsResponse> orderDetails;
    private Double total;
    private String status;
    private Date date;
}
