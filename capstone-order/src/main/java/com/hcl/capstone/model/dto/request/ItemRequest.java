package com.hcl.capstone.model.dto.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ItemRequest {
    private ProductRequest product;
    private Integer quantity;
    private Double price;
}
