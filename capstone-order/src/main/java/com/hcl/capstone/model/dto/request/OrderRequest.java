package com.hcl.capstone.model.dto.request;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class OrderRequest {
    private int userId;
    private List<ItemRequest> items;
    private double totalPrice;
    private int totalCount;
}
