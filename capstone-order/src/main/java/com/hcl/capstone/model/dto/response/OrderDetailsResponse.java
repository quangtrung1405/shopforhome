package com.hcl.capstone.model.dto.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OrderDetailsResponse {
    private String productName;
    private String image;
    private Double price;
    private Double totalPrice;
    private Integer quantity;
}
