package com.hcl.capstone.model.dto.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProductRequest {
    private Integer id;
    private String name;
    private double price;
    private String image;
    private int stock;
}
