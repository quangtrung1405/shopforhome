package com.hcl.capstone.service;

import com.hcl.capstone.model.dto.request.ItemRequest;
import com.hcl.capstone.model.dto.request.OrderRequest;
import com.hcl.capstone.model.dto.response.OrderDetailsResponse;
import com.hcl.capstone.model.dto.response.OrderResponse;
import com.hcl.capstone.model.entity.Order;
import com.hcl.capstone.model.entity.OrderDetails;
import com.hcl.capstone.repository.OrderDetailsRepository;
import com.hcl.capstone.repository.OrderRepository;
import com.hcl.capstone.utils.ConvertOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class OrderService {

    private OrderRepository orderRepository;
    private OrderDetailsRepository orderDetailsRepository;
    private ConvertOrder convertOrder;
    private WebClient.Builder webClientBuilder;

    @Autowired
    public OrderService(OrderRepository orderRepository, OrderDetailsRepository orderDetailsRepository, ConvertOrder convertOrder, WebClient.Builder webClientBuilder) {
        this.orderRepository = orderRepository;
        this.orderDetailsRepository = orderDetailsRepository;
        this.convertOrder = convertOrder;
        this.webClientBuilder = webClientBuilder;
    }

    public void createOrder(OrderRequest orderRequest) {
        Order order = Order.builder()
                .userId(orderRequest.getUserId())
                .total(orderRequest.getTotalPrice())
                .status("Processing")
                .date(new Timestamp(new Date().getTime()))
                .build();

        Order orderSaved = orderRepository.save(order);

        List<ItemRequest> items = orderRequest.getItems();

        List<OrderDetails> orderDetails = new ArrayList<>();

        items.forEach(item -> {
            orderDetails.add(
                    OrderDetails.builder()
                            .order(orderSaved)
                            .productId(item.getProduct().getId())
                            .price(item.getPrice())
                            .quantity(item.getQuantity())
                            .build()
            );

            webClientBuilder.build()
                    .patch()
                    .uri("http://localhost:8081/api/v1/update-quantity/" + item.getProduct().getId())
                    .header(MediaType.APPLICATION_JSON_VALUE)
                    .body(Mono.just(item.getQuantity()), Object.class)
                    .retrieve()
                    .bodyToMono(Object.class)
                    .block();
        });

        orderDetailsRepository.saveAll(orderDetails);
    }

    public List<OrderResponse> getOrderByUserId(Integer userId) {
        List<Order> orders = orderRepository.findOrderByUserId(userId);
        return convertOrder.convertOrderToOrderResponseList(orders);
    }

    public List<OrderResponse> getAllOrders() {
        List<Order> orders = orderRepository.findAll();
        return convertOrder.convertOrderToOrderResponseList(orders);
    }

    public List<OrderDetailsResponse> getOrderDetailsByOrder(Integer orderId) {
        Order order = orderRepository.findOrderById(orderId);
        List<OrderDetails> orderDetails = orderDetailsRepository.findOrderDetailsByOrder(order);
        return convertOrder.convertOrderDetailsToOrderDetailsResponseList(orderDetails);
    }

    public void updateStatusOrder(Integer orderId, String status) {
        Order order = orderRepository.findOrderById(orderId);
        order.setStatus(status);
        orderRepository.save(order);
    }

    public void deleteOrderById(Integer orderId) {
        orderRepository.deleteById(orderId);
    }
}
