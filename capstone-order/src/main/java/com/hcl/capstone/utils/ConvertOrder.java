package com.hcl.capstone.utils;

import com.hcl.capstone.model.dto.response.OrderDetailsResponse;
import com.hcl.capstone.model.dto.response.OrderResponse;
import com.hcl.capstone.model.dto.response.ProductResponse;
import com.hcl.capstone.model.entity.Order;
import com.hcl.capstone.model.entity.OrderDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.ArrayList;
import java.util.List;

@Component
public class ConvertOrder {

    @Autowired
    private WebClient.Builder webClientBuilder;

    public List<OrderResponse> convertOrderToOrderResponseList(List<Order> orders) {
        List<OrderResponse> orderResponses = new ArrayList<>();

        orders.forEach(e -> {
            OrderResponse orderResponse = OrderResponse.builder()
                    .orderId(e.getId())
                    .userId(e.getUserId())
                    .date(e.getDate())
                    .status(e.getStatus())
                    .total(e.getTotal())
                    .build();
            orderResponses.add(orderResponse);
        });

        return orderResponses;
    }

    public List<OrderDetailsResponse> convertOrderDetailsToOrderDetailsResponseList(List<OrderDetails> orderDetails) {
        List<OrderDetailsResponse> orderDetailsResponses = new ArrayList<>();

        orderDetails.forEach(e -> {
            ProductResponse productResponse = webClientBuilder.build()
                    .get()
                    .uri("http://localhost:8081/api/v1/product/" + e.getProductId())
                    .retrieve()
                    .bodyToMono(ProductResponse.class)
                    .block();

            OrderDetailsResponse orderDetailsResponse = OrderDetailsResponse.builder()
                    .productName(productResponse != null ? productResponse.getName() : null)
                    .image(productResponse != null ? productResponse.getImage() : null)
                    .price(productResponse != null ? productResponse.getPrice() : null)
                    .totalPrice(e.getPrice())
                    .quantity(e.getQuantity())
                    .build();
            orderDetailsResponses.add(orderDetailsResponse);
        });

        return orderDetailsResponses;
    }
}
