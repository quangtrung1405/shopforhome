package com.hcl.capstone.repository;

import com.hcl.capstone.model.entity.Order;
import com.hcl.capstone.model.entity.OrderDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderDetailsRepository extends JpaRepository<OrderDetails, Integer> {
    List<OrderDetails> findOrderDetailsByOrder(Order order);
}
