package com.hcl.capstone.repository;

import com.hcl.capstone.model.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {
    List<Order> findOrderByUserId(Integer userId);

    Order findOrderById(Integer orderId);

}
