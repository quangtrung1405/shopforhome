package com.hcl.capstone.controller;

import com.hcl.capstone.model.dto.request.OrderRequest;
import com.hcl.capstone.model.dto.response.MessageResponse;
import com.hcl.capstone.model.dto.response.OrderDetailsResponse;
import com.hcl.capstone.model.dto.response.OrderResponse;
import com.hcl.capstone.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(value = "http://localhost:4200")
@RequestMapping("/api/v1")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("/create-order")
    public ResponseEntity<Object> createOrder(@RequestBody OrderRequest orderRequest) {
        orderService.createOrder(orderRequest);
        return ResponseEntity.ok(new MessageResponse("Order successfully"));
    }

    @GetMapping("/order/{userId}")
    @ResponseBody
    public ResponseEntity<Object> getOrderByUserId(@PathVariable Integer userId) {
        List<OrderResponse> orderResponses = orderService.getOrderByUserId(userId);
        orderResponses.forEach(e -> {
            List<OrderDetailsResponse> orderDetailsResponses = orderService.getOrderDetailsByOrder(e.getOrderId());
            e.setOrderDetails(orderDetailsResponses);
        });
        return ResponseEntity.ok(orderResponses);
    }

    @GetMapping("/all-orders")
    public ResponseEntity<Object> getAllOrders() {
        List<OrderResponse> orderResponses = orderService.getAllOrders();
        orderResponses.forEach(e -> {
            List<OrderDetailsResponse> orderDetailsResponses = orderService.getOrderDetailsByOrder(e.getOrderId());
            e.setOrderDetails(orderDetailsResponses);
        });
        return ResponseEntity.ok(orderResponses);
    }

    @PatchMapping("/update-status/{orderId}")
    public ResponseEntity<Object> updateStatusOrder(@PathVariable Integer orderId, @RequestBody String status) {
        orderService.updateStatusOrder(orderId, status);
        return ResponseEntity.ok(new MessageResponse("Update status order successfully"));
    }

    @DeleteMapping("/delete-order/{orderId}")
    public ResponseEntity<Object> deleteOrder(@PathVariable Integer orderId) {
        orderService.deleteOrderById(orderId);
        return ResponseEntity.ok(new MessageResponse("Delete order successfully"));
    }
}
