package com.hcl.capstone.service;

import com.hcl.capstone.model.entity.ERole;
import com.hcl.capstone.model.entity.Role;
import com.hcl.capstone.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    public Optional<Role> findRoleByName(ERole name) {
        return roleRepository.findRoleByName(name);
    }
}
