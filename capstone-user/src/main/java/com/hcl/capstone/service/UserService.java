package com.hcl.capstone.service;

import com.hcl.capstone.model.dto.request.RegisterRequest;
import com.hcl.capstone.model.dto.request.UserRequest;
import com.hcl.capstone.model.dto.response.UserResponse;
import com.hcl.capstone.model.entity.ERole;
import com.hcl.capstone.model.entity.Role;
import com.hcl.capstone.model.entity.User;
import com.hcl.capstone.repository.RoleRepository;
import com.hcl.capstone.repository.UserRepository;
import com.hcl.capstone.utils.ConvertUser;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.UUID;

@Service
public class UserService {
    private UserRepository userRepository;
    private RoleRepository roleRepository;

    private WebClient.Builder webClientBuilder;
    private ConvertUser convertUser;

    @Autowired
    public UserService(UserRepository userRepository, RoleRepository roleRepository, WebClient.Builder webClientBuilder, ConvertUser convertUser) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.webClientBuilder = webClientBuilder;
        this.convertUser = convertUser;
    }

    public void register(RegisterRequest registerRequest) {
        User user = User.builder()
                .username(registerRequest.getUsername())
                .password(DigestUtils.sha256Hex(registerRequest.getPassword()))
                .email(registerRequest.getEmail())
                .phone(registerRequest.getPhone())
                .address(registerRequest.getAddress())
                .city(registerRequest.getCity())
                .build();

        String role = registerRequest.getRole();
        Role userRole;

        if ("admin".equals(role)) {
            userRole = roleRepository.findRoleByName(ERole.ROLE_ADMIN).orElseThrow(() -> new RuntimeException("Error: Role is not found"));
        } else {
            userRole = roleRepository.findRoleByName(ERole.ROLE_CUSTOMER).orElseThrow(() -> new RuntimeException("Error: Role is not found"));
        }

        user.setRole(userRole);

        User userRegistered = userRepository.save(user);

        Integer userId = userRegistered.getId();

        webClientBuilder.build()
                .post()
                .uri("http://localhost:8081/api/v1/create-wishlist/" + userId)
                .header(MediaType.APPLICATION_JSON_VALUE)
                .body(Mono.just(userId), Object.class)
                .retrieve()
                .bodyToMono(Object.class)
                .block();
    }

    public UserResponse login(String username, String password) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(password.getBytes());
        byte[] digest = md.digest();

        String hashPassword = DatatypeConverter.printHexBinary(digest);

        User user = userRepository.findUserByUsernameAndPassword(username, hashPassword);
        user.setToken(UUID.randomUUID().toString());

        userRepository.save(user);

        return convertUser.convertUserToUserResponse(user);
    }

//    public Optional findByToken(String token) {
//        User user = userRepository.findUserByToken(token);
//
//        return Optional.of(convertUser.convertUserToUserResponse(user));
//    }

    public UserResponse getUserById(Integer id) {
        User user = userRepository.findUserById(id);
        return convertUser.convertUserToUserResponse(user);
    }

    public List<User> getAllCustomers() {
        Role role = roleRepository.findRoleByName(ERole.ROLE_CUSTOMER).orElseThrow(() -> new RuntimeException("Error: Role is not found"));
        return userRepository.findUsersByRole(role);
    }

    public void updateUserProfile(UserRequest user, Integer id) {
        User updateUser = userRepository.findUserById(id);

        updateUser.setUsername(user.getUsername());
        updateUser.setEmail(user.getEmail());
        updateUser.setPhone(user.getPhone());
        updateUser.setAddress(user.getAddress());
        updateUser.setCity(user.getCity());

        userRepository.save(updateUser);
    }

    public void deleteUserById(Integer id) {
        userRepository.deleteById(id);
    }

    public Boolean existsUserByUsername(String username) {
        return userRepository.existsUserByUsername(username);
    }

    public Boolean existsUserByEmail(String email) {
        return userRepository.existsUserByEmail(email);
    }
}
