package com.hcl.capstone.repository;

import com.hcl.capstone.model.entity.Role;
import com.hcl.capstone.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    User findUserByUsernameAndPassword(String username, String password);

    @Query(nativeQuery = true, value = "SELECT LAST_INSERT_ID()")
    Integer getUserId();

    User findUserById(Integer id);

    User findUserByToken(String token);

    List<User> findUsersByRole(Role role);

    Boolean existsUserByUsername(String username);

    Boolean existsUserByEmail(String email);
}
