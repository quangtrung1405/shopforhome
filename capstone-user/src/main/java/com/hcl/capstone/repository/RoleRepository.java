package com.hcl.capstone.repository;

import com.hcl.capstone.model.entity.ERole;
import com.hcl.capstone.model.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
    Optional<Role> findRoleByName(ERole name);
}
