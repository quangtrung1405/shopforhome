package com.hcl.capstone.utils;

import com.hcl.capstone.model.dto.response.UserResponse;
import com.hcl.capstone.model.entity.User;
import org.springframework.stereotype.Component;

@Component
public class ConvertUser {

    public UserResponse convertUserToUserResponse(User user) {
        return UserResponse.builder()
                .id(user.getId())
                .username(user.getUsername())
                .email(user.getEmail())
                .phone(user.getPhone())
                .address(user.getAddress())
                .city(user.getCity())
                .role(user.getRole().getName())
                .token(user.getToken())
                .build();
    }
}
