package com.hcl.capstone.model.dto.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class WishListResponse {
    private Integer id;
    private Object products;
}
