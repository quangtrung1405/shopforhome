package com.hcl.capstone.model.entity;

public enum ERole {
    ROLE_CUSTOMER,
    ROLE_ADMIN
}
