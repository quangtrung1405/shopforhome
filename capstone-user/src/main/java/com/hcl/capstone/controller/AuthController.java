package com.hcl.capstone.controller;

import com.hcl.capstone.model.dto.request.LoginRequest;
import com.hcl.capstone.model.dto.request.RegisterRequest;
import com.hcl.capstone.model.dto.response.MessageResponse;
import com.hcl.capstone.model.dto.response.UserResponse;
import com.hcl.capstone.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.security.NoSuchAlgorithmException;

@RestController
@CrossOrigin(value = "http://localhost:4200")
@RequestMapping("/api/v1")
public class AuthController {

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public ResponseEntity<Object> register(@RequestBody RegisterRequest registerRequest) {
        if (userService.existsUserByUsername(registerRequest.getUsername())) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Username is already taken"));
        }

        if (userService.existsUserByEmail(registerRequest.getEmail())) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Email is already in use"));
        }

        userService.register(registerRequest);

        return ResponseEntity.ok(new MessageResponse("User registered successfully"));
    }

    @PostMapping("/login")
    public ResponseEntity<Object> login(@RequestBody LoginRequest loginRequest) {
        UserResponse user;
        try {
            user = userService.login(loginRequest.getUsername(), loginRequest.getPassword());

            if (user == null) {
                return ResponseEntity.badRequest().body(new MessageResponse("Error: Failed to login"));
            }
        } catch (NoSuchAlgorithmException e) {
            return ResponseEntity.internalServerError().body(new MessageResponse("Error: Internal Server Error"));
        }

        return ResponseEntity.ok(user);
    }
}
