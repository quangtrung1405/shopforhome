package com.hcl.capstone.model.dto.response;

import com.hcl.capstone.model.entity.Product;
import lombok.Builder;
import lombok.Data;

import java.util.List;


@Data
@Builder
public class WishListResponse {
    private Integer id;
    private List<Product> products;
}
