package com.hcl.capstone.service;

import com.hcl.capstone.model.dto.response.WishListResponse;
import com.hcl.capstone.model.entity.Product;
import com.hcl.capstone.model.entity.WishList;
import com.hcl.capstone.model.entity.WishListProduct;
import com.hcl.capstone.repository.WishListProductRepository;
import com.hcl.capstone.repository.WishListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class WishListProductService {
    private WishListProductRepository wishListProductRepository;
    private WishListRepository wishListRepository;

    @Autowired
    public WishListProductService(WishListProductRepository wishListProductRepository, WishListRepository wishListRepository) {
        this.wishListProductRepository = wishListProductRepository;
        this.wishListRepository = wishListRepository;
    }

    public Boolean existsProductInWishList(Integer productId) {
        return wishListProductRepository.existsByProductId(productId);
    }

    public WishListResponse getWishListByUserId(Integer userId) {
        WishList wishList = wishListRepository.findWishListByUserId(userId);

        List<WishListProduct> wishListProduct = wishListProductRepository.findWishListProductsByWishList(wishList);

        List<Product> products = new ArrayList<>();

        wishListProduct.forEach(e -> {
            products.add(e.getProduct());
        });

        return WishListResponse.builder().id(wishList.getId()).products(products).build();
    }

    @Transactional
    public void deleteProductInWishList(Integer productId) {
        wishListProductRepository.deleteWishListProductByProductId(productId);
    }
}
