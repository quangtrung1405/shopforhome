package com.hcl.capstone.service;

import com.hcl.capstone.model.entity.Product;
import com.hcl.capstone.model.entity.WishList;
import com.hcl.capstone.model.entity.WishListProduct;
import com.hcl.capstone.repository.ProductRepository;
import com.hcl.capstone.repository.WishListProductRepository;
import com.hcl.capstone.repository.WishListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WishListService {
    private WishListRepository wishListRepository;
    private WishListProductRepository wishListProductRepository;
    private ProductRepository productRepository;

    @Autowired
    public WishListService(WishListRepository wishListRepository, WishListProductRepository wishListProductRepository, ProductRepository productRepository) {
        this.wishListRepository = wishListRepository;
        this.wishListProductRepository = wishListProductRepository;
        this.productRepository = productRepository;
    }

    public void addProductToWishlist(Integer productId, Integer userId) {
        WishList wishList = wishListRepository.findWishListByUserId(userId);
        Product product = productRepository.findProductById(productId);

        wishListProductRepository.save(WishListProduct.builder().wishList(wishList).product(product).build());
    }

    public void createWishList(Integer userId) {
        wishListRepository.save(WishList.builder().userId(userId).build());
    }
}
