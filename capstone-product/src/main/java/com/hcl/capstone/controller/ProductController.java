package com.hcl.capstone.controller;

import com.hcl.capstone.model.dto.request.ProductRequest;
import com.hcl.capstone.model.entity.Product;
import com.hcl.capstone.model.dto.response.MessageResponse;
import com.hcl.capstone.service.FileUploadService;
import com.hcl.capstone.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:4200")
@RequestMapping("/api/v1")
public class ProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private FileUploadService fileUploadService;

    @GetMapping("/list-products")
    public ResponseEntity<Object> getAllProducts() {
        List<Product> products = productService.getAllProducts();

        return products.size() == 0 ? ResponseEntity.badRequest().body(new MessageResponse("Error: List is empty")) : ResponseEntity.ok(products);
    }

    @GetMapping("/product/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable Integer id) {
        Product product = productService.getProductById(id);
        return ResponseEntity.ok(product);
    }

    @PostMapping(value="/admin/add-product", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<Object> addNewProduct(@ModelAttribute ProductRequest productRequest) {
        String urlImage = MvcUriComponentsBuilder.fromMethodName(ProductController.class, "getFile", productRequest.getImage().getOriginalFilename()).build().toString();

        Product product = Product.builder()
                .name(productRequest.getName())
                .price(productRequest.getPrice())
                .stock(productRequest.getStock())
                .image(urlImage).build();
        productService.addNewProduct(product);
        fileUploadService.save(productRequest.getImage());
        return ResponseEntity.ok(new MessageResponse("Add new product successfully"));
    }

    @CrossOrigin("http://localhost:8081")
    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> getFile(@PathVariable String filename) {
        Resource file = fileUploadService.load(filename);

        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @PatchMapping("/update-quantity/{productId}")
    public ResponseEntity<Object> updateProductQuantity(@PathVariable Integer productId, @RequestBody Integer quantity) {
        productService.updateProductQuantity(productId, quantity);
        return ResponseEntity.ok(new MessageResponse("Update quantity successfully"));
    }

    @PatchMapping("/update-product/{productId}")
    public ResponseEntity<Object> updateProduct(@PathVariable Integer productId, @RequestBody ProductRequest productRequest) {
        productService.updateProduct(productId, productRequest);
        return ResponseEntity.ok(new MessageResponse("Update product " + productId + " successfully"));
    }

    @DeleteMapping("/admin/delete-product/{productId}")
    public ResponseEntity<Object> deleteProductById(@PathVariable Integer productId) {
        productService.deleteProductById(productId);
        return ResponseEntity.ok(new MessageResponse("Delete product " + productId + " successfully"));
    }
}
