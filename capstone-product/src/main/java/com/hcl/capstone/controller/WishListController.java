package com.hcl.capstone.controller;

import com.hcl.capstone.model.dto.response.MessageResponse;
import com.hcl.capstone.model.dto.response.WishListResponse;
import com.hcl.capstone.service.WishListProductService;
import com.hcl.capstone.service.WishListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(value = "http://localhost:4200")
@RequestMapping("/api/v1")
public class WishListController {
    @Autowired
    private WishListService wishListService;

    @Autowired
    private WishListProductService wishListProductService;

    @PostMapping("/create-wishlist/{userId}")
    public ResponseEntity<Object> createWishList(@PathVariable Integer userId) {
        wishListService.createWishList(userId);
        return ResponseEntity.ok(new MessageResponse("Create wishlist successfully"));
    }

    @PostMapping("/add-wishlist/{userId}/{productId}")
    public ResponseEntity<Object> addProductToWishlist(@PathVariable Integer productId, @PathVariable Integer userId) {
        if (wishListProductService.existsProductInWishList(productId)) {
            return ResponseEntity.badRequest().body(new MessageResponse("Err: Product already in wishlist"));
        }

        wishListService.addProductToWishlist(productId, userId);
        return ResponseEntity.ok(new MessageResponse("Add product to wishlist successfully"));
    }

    @GetMapping("/get-wishlist/{userId}")
    public ResponseEntity<Object> getWishList(@PathVariable Integer userId) {
        WishListResponse wishListResponse = wishListProductService.getWishListByUserId(userId);
        return ResponseEntity.ok(wishListResponse);
    }

    @DeleteMapping("/delete-wishlist/{productId}")
    public ResponseEntity<Object> deleteProductInWishList(@PathVariable Integer productId) {
        wishListProductService.deleteProductInWishList(productId);
        return ResponseEntity.ok(new MessageResponse("Delete product " + productId + " in wishlist successfully"));
    }
}
